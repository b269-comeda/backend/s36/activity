// Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");

// Server setup
const app = express();
const port = 3001;

// This allows us to use all the routes defined in "taskRoute.js"
const taskRoute = require("./routes/taskRoute");

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Database connection
mongoose.connect("mongodb+srv://jeremiahcomeda:admin123@zuitt-bootcamp.q8eipm4.mongodb.net/s36?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

mongoose.connection.once("open", () => console.log(`Now connected to the cloud database`));

// Add task route
// Allows all the task routes created in the "taskRoute.js" file to use "/tasks" route
app.use("/tasks", taskRoute);

app.listen(port, () => console.log(`Now listening to port ${port}.`));