// Defines WHEN particularly controllers will be used 
// Contain all the endpoints and responses that we can get from controllers

const express = require("express");
// Creates a router instance that functions as a middleware and routing system
const router = express.Router();

const taskController = require("../controllers/taskController");

// Route to get all the tasks
// http://localhost:3001/tasks
router.get("/", (req, res) => {
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
});

// Route to create task
router.post("/", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
});

// Route to delete task
router.delete("/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
});

// [SECTION] ACTIVITY STARTS HERE
// route for getting a specific task
router.get("/:id", (req, res) => {
	taskController.getATask(req.params.id).then(resultFromController => res.send(resultFromController));
});

// route for changing the status of a task to complete
router.put("/:id/complete", (req, res) => {
	taskController.updatingStatusOfATask(req.params.id).then(resultFromController => res.send(resultFromController));
});

// [SECTION] ACTIVITY ENDS HERE

module.exports = router;